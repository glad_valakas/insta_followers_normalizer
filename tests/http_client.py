import requests


URL = "http://localhost/maxl/insta_followers_normalizer/normalize.php"
def post_normalize(count, deviations_range):
    payload = {'count': count, 'range': deviations_range, 'random_fraction':15,'table_name': 'new_insta_followers'}
    response = requests.post(URL, data=payload)
    response.raise_for_status()
    return response.text
