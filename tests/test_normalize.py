import logging

from . import engine, DATABASE_NAME, http_client
from .models import Base, InstagramFollowers
from sqlalchemy.orm import sessionmaker


FOLLOWERS = [
    71918,
    71847,
    71774,
    71702,
    71630,
    71555,
    71482,
    71415,
    71345,
    71341,
    71340,
    71340,
]
FOLLOWERS = FOLLOWERS[::-1]
test_data = [{'followers':71454, 'difference':54}]
test_data.extend({'followers': FOLLOWERS[i+1],
              'difference': FOLLOWERS[i+1] - FOLLOWERS[i]}
              for i in range(len(FOLLOWERS)-1))



class TestInstaFollowersNormalizer:

    @classmethod
    def setup_class(cls):
        engine.execute("DROP DATABASE IF EXISTS %s"%DATABASE_NAME)
        engine.execute("CREATE DATABASE insta_followers_normalize_test")
        engine.execute("USE %s"%DATABASE_NAME)
        Base.metadata.create_all(engine)

        Session = sessionmaker()
        sess = Session(bind=engine)
        for row in test_data:
            sess.add(InstagramFollowers(**row))
            sess.commit()
        sess.close()

    def test(self, capsys):
        capsys.disabled()
        print_table_rows()
        print("+++++++++++++++++++++++++++++")
        print(http_client.post_normalize(7, 10))
        print("+++++++++++++++++++++++++++++")
        print_table_rows()

    @classmethod
    def teardown_class(cls):
        engine.execute("DROP DATABASE %s"%DATABASE_NAME)


def print_table_rows():
    sess = sessionmaker()(bind=engine)
    current_rows = sess.execute("SELECT * FROM new_insta_followers").fetchall()
    print()
    format = '%-5s|%-9s|%-8s'
    print(format%tuple(current_rows[0].keys()))
    print('-------------------------')
    for row in current_rows[::-1]:
        print(format%tuple(row.values()))
    sess.close()  
