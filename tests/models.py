import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class InstagramFollowers(Base):
	__tablename__ = 'new_insta_followers'

	id = sa.Column(sa.Integer, primary_key=True)
	followers = sa.Column(sa.BigInteger)
	difference = sa.Column(sa.Integer)
