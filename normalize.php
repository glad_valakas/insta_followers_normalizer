<?php
if( $_POST["count"] && $_POST["range"] && $_POST["table_name"]) {
if (is_numeric($_POST["count"]) && is_numeric($_POST["range"])) {
   $database_name = "insta_followers_normalize_test";
   $host = "localhost";
   $db_user = "root";
   $db_pass = "";

   $primary_key_row = "id";
   $followers_count_row = "followers";
   $flollowers_difference_row = "difference";

   $random_fraction = empty($_POST['random_fraction']) ? '15' : $_POST['random_fraction'];
   if (!is_numeric($random_fraction)) die("Random fraction must be numeric.");

   echo "Start followers normalizing. Range: ". $_POST['range']. " Rows number: ". $_POST['count']. "<br>\n";

   $conn = new mysqli($host, $db_user, $db_pass, $database_name);

   if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
   }

   $table_name = mysqli_real_escape_string($conn, $_POST["table_name"]);

   $result = $conn->query("SELECT ".$primary_key_row.", ".$followers_count_row.", ".$flollowers_difference_row." FROM ".$table_name." ORDER BY ".$primary_key_row." DESC LIMIT ".$_POST["count"]) or die("Internal Server Error.");


   if ($_POST["count"] != $result->num_rows){
      die("There are only ".$result->num_rows." rows.");
   }

   $differences = array();
   $followers = array();
   $pks = array();

   //fetch rows
   for ($i = 0; $i < $result->num_rows; $i++){
      $row = $result->fetch_array(MYSQLI_NUM);
      $pks[] = $row[0];
      $followers[] = $row[1];
      $differences[] = $row[2];
   }

   list($new_followers_count, $new_differences) = calc_new_rows($followers, $differences, $_POST["range"], $random_fraction);

   echo "New followers count was calculated successfully.";

   $conn->begin_transaction();
   try {
      for ($i = 0; $i < count($new_differences); $i++){
         if(!$conn->query("UPDATE ".$table_name." SET ".$followers_count_row." = ".$new_followers_count[$i].", ".$flollowers_difference_row." = ".$new_differences[$i]." WHERE ".$primary_key_row." = ".$pks[$i])){
            $conn->rollback();
            die("Internal Server Error.");
         }
      }
   } catch (Exception $e) {
         $conn->rollback();
         die("Internal Server Error.");
   }
   $conn->commit();
   
   echo "Done.";
}
// count or range isn't numeric
exit();
}
function calc_new_rows($followers_count, $differences, $range, $random_fraction) {
   $rows_count = count($followers_count);
   $real_range = max($differences) - min($differences);
   $diff_average = array_sum($differences)/count($differences);
   $total_difference = $followers_count[0] - $followers_count[$rows_count-1];
   $maximum_random_deviation = $total_difference * ($random_fraction / 100) / $rows_count;
   // echo $total_difference."\n";
   // echo $maximum_random_deviation."\n";
   $deviation_accumulator = 0;

   $new_differences = array();
   $new_followers_count = array();

   foreach ($differences as $diff){

      $deviation_addendum = rand(0, $maximum_random_deviation) - $deviation_accumulator;
      // echo "diff:".$diff."\n";
      $diff += $deviation_addendum;
      $deviation_accumulator = $deviation_addendum;
      // echo "deviation_addendum:".$deviation_addendum."\n";
      // echo "deviation_accumulator:".$deviation_accumulator."\n";
      // echo "new diff:".$diff."\n";
      $new_differences[] = round($diff_average+$range*($diff-$diff_average)/$real_range);
      // echo $new_differences[count($new_differences)-1]."\n";
   }

   //set first array value of followers count (last in table) must be equal previous value
   $new_followers_count[] = $followers_count[0];
   //calculate other values of followers with new difference values
   for ($i = 1; $i < count($differences); $i++){
      $new_followers_count[] = $new_followers_count[$i-1] - $new_differences[$i-1];
   }

   // Recalculate last difference (due to rounding may decreased value by 1)
   // echo $new_differences[$rows_count-1]."\n";
   $new_differences[$rows_count-1] = $differences[$rows_count-1] - $followers_count[$rows_count-1] + $new_followers_count[$rows_count-1];

   return array($new_followers_count, $new_differences);
}
?>
<html>
   <body>
      <form action = "<?php $_PHP_SELF ?>" method = "POST">
         Records count: <input type="number" name="count" />
         Range: <input type="number" name = "range" value="10" />
         Random fraction in percent: <input type="number" name = "random_fraction" value="15" />
         Table name <input name = "table_name" value="" />
         <input type = "submit" />
      </form>
   </body>
</html>
