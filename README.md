# insta_followers_normalizer

## Installation guide

Clone the repository:
```bash
git clone git@bitbucket.org:glad_valakas/insta_followers_normalizer.git
```
Install php:
```bash
sudo add-apt-repository ppa:ondrej/php
sudo apt update
sudo apt install php5.6 php5.6-mysql
```
Install and configure apache2:
```bash
sudo apt install apache2
printf 'Alias "/maxl/" "<path_to_the_script>"
<Directory <path_to_the_script>>
     Options Indexes FollowSymLinks
     AllowOverride None
     Require all granted
</Directory>\n' | sudo tee /etc/apache2/conf-available/maxl.conf
sudo a2enconf maxl
sudo service apache2 restart
```
install python requirements:
```bash
cd tests
pip3 install -r requirements.txt
```
run tests:
```bash
pytest -s
```